import { SortComponent } from './sort/sort.component';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'sort', component: SortComponent}
    // otherwise redirect to home
];

export const appRoutingModule = RouterModule.forRoot(routes);