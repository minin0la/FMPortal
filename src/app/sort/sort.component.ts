import { Component } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.css']
})
export class SortComponent  {
	public num1:any;
	public num2:number;
	public result:any;
	public resultheading: any;

	sort(){
		const regex = /(^.{3})(?:[\s])(.*)(?:[\s]-[\s])(?:(?=.+(?:[\s])\(.+\)\/.+(?:[\s])\(.+\))(.+)(?:[\s])\((.+)\)\/(.+)(?:[\s])\((.+)\)|(?:(?=.+(?:[\s])\(.+\)\/.+)(.+)(?:[\s])\((.+)\)\/(.+)|(?:(?=.+\/.+(?:[\s])\(.+\))(.+)\/(.+)(?:[\s]\((.+)\))|(?:(?=[^\s]+\/.+)(.+)\/(.+)|(?:(?=.+(?:[\s])\(.+\))(.+)(?:[\s])\((.+)\)|(.+))))))/gm;
		let x;
		let theresult;
		var final_list = [];
		while ((x = regex.exec(this.num1)) !== null) {
			if (x[3] && x[5]) {
                final_list.push(
                    {
                        'Rank': x[1],
                        'Name': x[2],
                        'AM': x[3],
                        'AM_REASON': '(' + x[4] + ')',
                        'PM': x[5],
                        'PM_REASON': '(' + x[6] + ')',
                    }
                );
            } else if (x[7] && x[9]) {
                final_list.push(
                    {
                        'Rank': x[1],
                        'Name': x[2],
                        'AM': x[7],
                        'AM_REASON': '(' + x[8] + ')',
                        'PM': x[9],
                        'PM_REASON': '',
                    }
                );
            } else if (x[10] && x[11]) {
                final_list.push(
                    {
                        'Rank': x[1],
                        'Name': x[2],
                        'AM': x[10],
                        'AM_REASON': '',
                        'PM': x[11],
                        'PM_REASON': '(' + x[12] + ')',
                    }
                );
            } else if (x[15]) {
                final_list.push(
                    {
                        'Rank': x[1],
                        'Name': x[2],
                        'AM': x[15],
                        'AM_REASON': '(' + x[16] + ')',
                        'PM': x[15],
                        'PM_REASON': '(' + x[16] + ')',
                    }
                );
            } else if (x[13] && x[14]) {
                final_list.push(
                    {
                        'Rank': x[1],
                        'Name': x[2],
                        'AM': x[13],
                        'AM_REASON': '',
                        'PM': x[14],
                        'PM_REASON': '',
                    }
                );
            } else if (x[17]) {
                final_list.push(
                    {
                        'Rank': x[1],
                        'Name': x[2],
                        'AM': x[17],
                        'AM_REASON': '',
                        'PM': x[17],
                        'PM_REASON': '',
                    }
                );
            }
		}
		// console.log(final_list)
		var statelist = [];
        var choice = '';
		// for (var i = 0; i < final_list.length; i++){
		// 	console.log(final_list[i])
		// }
		for (var i = 0; i < final_list.length; i++){
			statelist.push(final_list[i].AM)
			statelist.push(final_list[i].PM)
		}
        statelist = [...new Set(statelist)]
        final_list.sort((a, b) => a.Name.localeCompare(b.Name));
        final_list.sort((a, b) => a.Rank.localeCompare(b.Rank));
        var ele = document.getElementsByTagName('input');
        for(i = 0; i < ele.length; i++) { 
            if(ele[i].type="radio") { 
                if(ele[i].checked){
                choice = ele[i].value
                }
            }
        }
        var namelist = '';
		for(var o = 0; o < statelist.length; o++){
            var temp_namelist = '';
            var counter = 0;
			for (var i = 0; i < final_list.length; i++){
				if(final_list[i][choice] == statelist[o]){
                    temp_namelist = temp_namelist + final_list[i].Rank + ' ' + final_list[i].Name + ' - ' + final_list[i][choice] + '\n';
                    counter++;
				}
			}
			namelist = namelist + statelist[o] + ' (' + counter + ')\n' + temp_namelist + '\n'
		}
        this.result = namelist
		// console.log(thename)
		// for (var key in final_list[0]){
		// 	theresult = theresult + final_list[key] + ' \n'
		// }
		// console.log(final_list)
	}

	

}