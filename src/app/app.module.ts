import { RouterModule, Routes } from '@angular/router';
// import { appRoutingModule } from './app.routing';
// import { AuthService } from './shared/auth.service';

//Firebase Module
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
// import { AngularFireAuthModule } from '@angular/fire/auth';

import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { HomeComponent } from './home/home.component';
import { SortComponent } from './sort/sort.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'sort', component: SortComponent}
  // otherwise redirect to home
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SortComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase, 'FMPortal'),
    AngularFirestoreModule,
    // AngularFireAuthModule,
    AngularFireStorageModule,
    NgbModule,
    FormsModule,
    ClipboardModule,
    RouterModule.forRoot(routes)
  ],
  // providers: [AuthService],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
