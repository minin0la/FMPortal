// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBP2GvLwD4LXRBiiKHJ_OaaZGhSbKYQPpY",
    authDomain: "fmportal-aa71c.firebaseapp.com",
    databaseURL: "https://fmportal-aa71c.firebaseio.com",
    projectId: "fmportal-aa71c",
    storageBucket: "fmportal-aa71c.appspot.com",
    messagingSenderId: "465783011821",
    appId: "1:465783011821:web:ce2e67ae93c8f598088fe4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
