import { browser, by, element } from 'protractor';

export class FMPortalPage {
  navigateTo() {
    return browser.get('/');
  }

  getTitleText() {
    return element(by.css('app-root h3')).getText() as Promise<string>;
  }
}
