import { FMPortalPage } from './app.po';
import { browser, logging } from 'protractor';

describe('FMPortal App', () => {
  let page: FMPortalPage;

  beforeEach(() => {
    page = new FMPortalPage();
  });

  it('should display title FMPortal', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('What would you like to do today?');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
